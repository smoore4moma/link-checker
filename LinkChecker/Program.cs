﻿using System;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.IO;
using System.Data;
using System.Configuration;

namespace LinkChecker
{
    class Program
    {

        public static bool m_fileexist;
        public static StreamWriter m_results = new StreamWriter(ConfigurationManager.AppSettings["errors"], true);
        public static string[] m_files_urls = File.ReadAllLines(ConfigurationManager.AppSettings["links"]);

        static void Main(string[] args)
        {
            // Select mode
            string m_mode = ConfigurationManager.AppSettings["mode"].ToUpper();

            if (m_mode == "FILE") { CheckFiles(); }
            if (m_mode == "URL") { CheckUrls(); }
        }


        public static void CheckFiles()
        {
            for (int i = 0; i < m_files_urls.Length; i++)
            {
                m_fileexist = RemoteFileExists(m_files_urls[i]);

                if (m_fileexist == false)
                {
                  Console.WriteLine(m_files_urls[i].ToString() + "," + m_fileexist);
                  m_results.WriteLine(m_files_urls[i].ToString());
                }
                m_results.Flush();
            }
        }

        public static void CheckUrls()
        {
            for (int i = 0; i < m_files_urls.Length; i++)
            {
                m_fileexist = RemoteUrlExists(m_files_urls[i]);

                if (m_fileexist == false)
                {
                    Console.WriteLine(m_files_urls[i].ToString() + "," + m_fileexist);
                    m_results.WriteLine(m_files_urls[i].ToString() + "," + m_fileexist);
                }
                m_results.Flush();
            }
        }

        public static bool RemoteFileExists(string file)
        {
            try
            {
                bool IsExistsFile = System.IO.File.Exists(file);

                if (!IsExistsFile)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static bool RemoteUrlExists(string url)
        {
            try
            {

                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                request.Method = "HEAD";
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                //Returns TRUE if the Status code == 200
                response.Close();
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch
            {
                //Any exception will return false.
                return false;
            }

        }
    }
}

