#link-checker#

##Instructions##

First, create a list of TMS files with this query (note: just 100 to test):

SELECT TOP 100 path = mp.[Path] + mf.[FileName]
FROM [MediaFiles] mf
INNER JOIN [MediaPaths] mp ON mf.PathID = mp.PathID

Save the results as a .txt file.

Download two files from here (they are safe):

https://bitbucket.org/smoore4moma/link-checker/downloads/

Save the files in the same folder.  The file called LinkChecker.exe.config has the settings below.  Open in Notepad and edit as needed.

    <appSettings>
      <!-- mode choices are "FILE" or "URL" -->
      <add key="mode" value="FILE" />
      <add key="links" value="C:\Temp\files.txt" />
      <add key="errors" value="C:\Temp\errors.txt" />
    </appSettings> 

- mode: if you are checking files put FILE.  Use URL for web links.
- links:  this is the list of files to check (ie, results from the SQL above)
- errors: when done, this will have the list of files that do not exist where the database thinks they should be

"links" and "errors" can be named whatever you want and can point to wherever you want.

Open the path to LinkChecker.exe in a command prompt. and run it like this...

C:\MyMuseum\Misc>LinkChecker.exe

Check the errors file when it is done.